CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration


INTRODUCTION
------------

Cache URL Query Ignore improves the performance of the URL cache context
by removing query parameters from the URL cache context key.

Cache URL Query Ignore is the sibling module of [Page Cache Query Ignore](https://www.drupal.org/project/page_cache_query_ignore), 
which affects only page cache.  
This module affects all usages of the URL cache context.

As an example: with the Drupal core page_cache implementation, the following
URLs will be considered different.

 * https://my.site/
 * https://my.site/?gclid=abc

By setting the ignored query parameters to `gclid`, these URLs will be
considered identical by the cache and performance will be greatly improved. This
will also reduce the size of your cache.


Credits to the authors of [Page Cache Query Ignore](https://www.drupal.org/project/page_cache_query_ignore)
for their solution of the query parameter problem, on which this solution is
heavily based.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  [Installing Modules](https://www.drupal.org/node/1897420) for further
  information.


CONFIGURATION
-------------

* Install this module, go to the module configuration page.
* Provide a listing of query parameters to ignore in the _Ignored query parameters_ field and
  the desired ignore action.
