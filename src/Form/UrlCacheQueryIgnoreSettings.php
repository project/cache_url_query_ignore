<?php

declare(strict_types=1);

namespace Drupal\cache_url_query_ignore\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Module settings form.
 */
class UrlCacheQueryIgnoreSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'cache_url_query_ignore_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['cache_url_query_ignore.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('cache_url_query_ignore.settings');

    $form['query_parameters'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Query parameters'),
      '#required' => TRUE,
      '#default_value' => implode("\n", $config->get('query_parameters')),
      '#description' => $this->t('List of query parameters to exclude or include (one parameter per line).'),
    ];
    $form['ignore_action'] = [
      '#type' => 'radios',
      '#title' => $this->t('Ignore action'),
      '#required' => TRUE,
      '#options' => [
        'exclude' => $this->t('Exclude these query parameters from cache key'),
        'include' => $this->t('Include only these query parameters in the cache key'),
      ],
      '#default_value' => $config->get('ignore_action'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('cache_url_query_ignore.settings');
    $parameters = array_map('trim', explode("\n", $form_state->getValue('query_parameters')));
    $config->set('query_parameters', array_filter($parameters));
    $config->set('ignore_action', $form_state->getValue('ignore_action'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
